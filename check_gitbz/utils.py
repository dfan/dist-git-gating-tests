import datetime
import re

BZRE = re.compile(r"(?:(?:bug|bz|rhbz)\s*#?|#)\s*(\d+)", re.IGNORECASE)
CVERE = re.compile(r"(CVE-[0-9]{4}-[0-9]{4,})", re.IGNORECASE)
# The (?!...) section at the end makes sure the identifier is not followed by a
# dash or a digit. Otherwise CVEs would match as well.
JIRARE = re.compile(r"[A-Z]+-[0-9]+(?!-|[0-9])")


def get_commit_files(commit, no_renames=False):
    """Return a list of all the files involved in a commit obj"""
    args = ["--name-status", "--no-commit-id", "-r", commit.hexsha]
    if no_renames:
        # Detect renames. They are indicated in the output by lines starting
        # with R. Those lines will be filtered below.
        args.append("-M")
    files = commit.repo.git.diff_tree(*args)
    return [f.split(None, 1)[1] for f in filter(None, files.split("\n")) if f[0] != "R"]


def get_commit_message(commit):
    """Return the commit message as a list of lines"""
    return list(filter(None, commit.message.split("\n")))


def get_tag_message(tag):
    """Return the commit message as a list of lines"""
    return list(filter(None, tag.message.split("\n")))


def get_author_date(commit):
    """Return a date string of when the author created the commit"""
    timestamp = commit.authored_date
    author_date = datetime.datetime.fromtimestamp(timestamp)
    return str(author_date)


def get_commit_date(commit):
    """Return a date string of when the commit was done"""
    timestamp = commit.committed_date
    commit_date = datetime.datetime.fromtimestamp(timestamp)
    return str(commit_date)


def get_tagged_date(tag):
    """Return a date string of when the author created the commit"""
    timestamp = tag.tagged_date
    tagged_date = datetime.datetime.fromtimestamp(timestamp)
    return str(tagged_date)


def get_commits(repo, shaold, shanew):
    """Get all commits between an old shasum and a new one"""
    # See if this is a new branch
    if shaold == "0000000000000000000000000000000000000000":
        # Get all commits on the branch
        commits = repo.iter_commits(shanew)
    else:
        commits = repo.iter_commits("%s..%s" % (shaold, shanew))
    return list(commits)


def get_charset(text):
    text_charset = None
    for text_charset in "US-ASCII", "ISO-8859-1", "UTF-8":
        try:
            text.encode(text_charset)
        except UnicodeError:
            pass
        else:
            break

    return text_charset


def specAdditions(module, commit):
    """Grab all the additions to the spec file from a given commit obj."""
    specfile = "%s.spec" % module
    # Test for the existance of the file in git here.
    if specfile not in get_commit_files(commit, no_renames=True):
        return []

    specdiff = commit.repo.git.show(
        commit.hexsha, "--", specfile, stdout_as_string=False
    )
    # Decote the diff as UTF-8, but replace invalid bytes. This should survive
    # any input thrown at it. Since bugzilla references are ASCII only, it
    # should not have any effect on functionality.
    specdiff = specdiff.decode("utf-8", "replace")
    additions = [line[1:] for line in specdiff.split("\n") if line.startswith("+")]
    return additions


def bugzillaIDs(prefix, lines):
    """
    Extract unique Bugzilla ID numbers from the given text.
    Return a list of unique integer IDs.  If there are no Bugzilla
    IDs, return an empty list.

    This is part of API for dist-git-policy.
    """
    bzs = {}

    lineRE = re.compile(r"^\s*-?\s*%s" % prefix, re.IGNORECASE)

    for line in lines:
        if lineRE.search(line):
            for bzID in BZRE.findall(line):
                try:
                    bzID = int(bzID)
                    bzs[bzID] = 1
                except ValueError:
                    pass
            for cveID in CVERE.findall(line):
                bzs[cveID] = 1
            for jiraID in JIRARE.findall(line):
                bzs[jiraID] = 1

    return list(bzs.keys())


def get_commit_data(module, commit, resolves=True, related=True, reverts=True):
    """Given a commit object, return a dict with details about it and modified
    lines in specfile and commit message.
    """
    result = {"hexsha": commit.hexsha, "files": get_commit_files(commit)}
    speclines = specAdditions(module, commit)
    message = get_commit_message(commit)
    text = speclines + message
    if resolves:
        result["resolved"] = bugzillaIDs("Resolves?:", text)
    if related:
        result["related"] = bugzillaIDs("Related?:", text)
    if reverts:
        result["reverted"] = bugzillaIDs("Reverts?:", text)
    return result, speclines, message
